module.exports = {
  env: {
    es6: true,
    node: true,
  },
  extends: ['standard', 'plugin:jest/recommended', 'plugin:prettier/recommended'],
  rules: {
    'no-console': 'error',
    'no-debugger': 'error',
    'no-unused-vars': ['error', { args: 'all', argsIgnorePattern: '(^_|resolve|reject)' }],
  },
}
