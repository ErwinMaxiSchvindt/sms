require('../config/load-config')
const { getDb } = require('../src/db/connection')
const smsSchema = require('../src/db/schemas/sms')

async function updateSchemas() {
  const db = await getDb()

  const updateSchema = async (coll, schema, level) => {
    await db.command({
      collMod: coll,
      validator: schema,
      validationLevel: level,
      validationAction: 'error',
    })
    // eslint-disable-next-line no-console
    console.log(`Updated schema in collection ${coll}`)
  }

  const triples = [['sms', smsSchema, 'moderate']]
  await Promise.all(triples.map((t) => updateSchema(...t)))
  process.exit()
}

updateSchemas()
