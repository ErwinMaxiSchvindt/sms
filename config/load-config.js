process.env.NODE_ENV = process.env.NODE_ENV || 'development'

const credentials = require(`../config/${process.env.NODE_ENV}/credentials`)
const settings = require(`../config/${process.env.NODE_ENV}/settings`)

process.env = {
  ...process.env,
  ...credentials,
  ...settings,
}
