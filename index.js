require('./config/load-config')

const configureExpressApp = require('./src/endpoints/configure-express-app')
const { logInfo, logError } = require('./src/gateways/log')

const app = configureExpressApp()

startServer()
    .then(server => { logInfo(`Server up and running at ${server.address().port} 🚀`) })
    .catch(error => { logError(`Error while starting SMS server: ${error}`), process.exit(2) })

function startServer() {
    return new Promise((resolve, reject) => {
        const server = app
            .listen(app.get('port'), () => { resolve(server) })
            .on('error', error => { reject(error) })
    })
}
