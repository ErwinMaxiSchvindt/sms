const express = require('express')
const nocache = require('nocache')
const morgan = require('morgan')

const endpoints = require('./endpoints')
const validationErrorHandler = require('./middlewares/validation-error-handler')
const errorHandler = require('./middlewares/error-handler')

module.exports = () => {
  const app = express()
  app.set('port', process.env.PORT || 4000)

  if (process.env.NODE_ENV !== 'production') app.use(morgan('combined'))

  app.use(nocache())
  app.use(express.urlencoded({ extended: false }))
  app.use(express.json())

  app.use('/api/ping', (_, res) => res.sendStatus(204))

  app.use('/api/', endpoints())

  app.use(validationErrorHandler)
  app.use((_, __, next) => next(new Error('The URL was not found')))
  app.use(errorHandler)

  return app
}
