const { Router } = require('express')
const asyncMiddleware = require('../middlewares/async-middleware')
const { saveSmsValidations } = require('../../services/sms/validations')
const sendMessage = require('../../services/sms/send-sms')

const route = Router()

module.exports = (app) => {
  app.use('/sms/', route)

  route.post(
    '/send',
    saveSmsValidations,
    asyncMiddleware(async (req, res) => {
      const result = await sendMessage(req.body)
      res.json(result)
    }),
  )
}
