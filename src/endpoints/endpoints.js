const { Router } = require('express')

const sms = require('./routes/sms')

module.exports = () => {
  const app = Router()

  sms(app)

  return app
}
