const { logError } = require('../../gateways/log')
/* istanbul ignore file */
module.exports = (error, _, res, _next) => {
  res.status(500)
  // eslint-disable-next-line no-console
  logError(error, 500)
  res.json({ error: { status: 500, errMessage: error.message, errStack: error.stack } })
}
