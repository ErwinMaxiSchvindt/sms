const { isCelebrateError } = require('celebrate')
const { logError } = require('../../gateways/log')

module.exports = (error, req, res, next) => {
  if (!isCelebrateError(error)) return next(error)

  /* istanbul ignore else */
  if (error.details.size) {
    const details = error.details.get('body')
      ? error.details.get('body')
      : error.details.get('query')
    const status = 422
    logError('Validation error', status, req)
    res.status(status).json({
      error: {
        status: status,
        validations: details,
      },
    })
  } else {
    next(error)
  }
}
