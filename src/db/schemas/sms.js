module.exports = {
  $jsonSchema: {
    title: 'Sms',
    properties: {
      _id: { bsonType: 'objectId' },
      phoneNumber: { bsonType: 'string' },
      message: { bsonType: 'string' },
      countryIso: { bsonType: 'string' },
      status: { bsonType: 'string' },
      sent: { bsonType: 'bool' },
      createdAt: { bsonType: 'date' },
      updatedAt: { bsonType: 'date' },
    },
    required: ['phoneNumber', 'message', 'countryIso', 'status'],
  },
}
