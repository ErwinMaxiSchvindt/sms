const { logError } = require('../gateways/log')
const { MongoClient } = require('mongodb')

const db = process.env.db
const dbName = db.name
const connectionUrl = db.connectionUrl

const mongoClientConfig = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
}

module.exports = {
  getClient,
  getDb,
}

process.on('SIGINT', closeClient)
process.on('SIGTERM', closeClient)

const mongoClient = new MongoClient(connectionUrl, mongoClientConfig)

let connectionAttempt

async function connectClient() {
  if (connectionAttempt) return connectionAttempt

  try {
    connectionAttempt = await mongoClient.connect()
  } catch (error) {
    logError('Could not connect to MongoDB %o', error)
    process.exit(1)
  }
}

async function closeClient() {
  if (connectionAttempt) await connectionAttempt

  try {
    return mongoClient.close()
  } catch (error) {
    logError('Could not close clients %o', error)
    process.exit(1)
  }
}

async function getDb() {
  await connectClient()
  return mongoClient.db(dbName)
}

async function getClient() {
  await connectClient()
  return mongoClient
}
