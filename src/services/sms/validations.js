const { celebrate, Joi } = require('celebrate')

const saveSmsValidations = celebrate({
  body: Joi.object({
    phoneNumber: Joi.string().min(5).required(),
    message: Joi.string().required(),
    countryIso: Joi.string().length(2).required(),
  }),
})

module.exports = {
  saveSmsValidations,
}
