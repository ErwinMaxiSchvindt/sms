const getCollections = require('../../gateways/get-collections')
const sendMessage = require('../../gateways/send-message')

module.exports = async ({ phoneNumber, message, countryIso }) => {
  phoneNumber = phoneNumber.replace(/\s/g, '')
  const { Sms } = await getCollections()

  const result = await sendMessage(phoneNumber, message)

  if (process.env.saveMessage) {
    const sent = result.sent
    const status = result.status
    const date = new Date()

    await Sms.insertOne({
      phoneNumber,
      message,
      countryIso,
      status,
      sent,
      createdAt: date,
      updatedAt: date,
    })
  }

  return result
}
