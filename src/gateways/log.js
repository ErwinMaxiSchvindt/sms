require('console-error')
require('console-info')
require('console-warn')

module.exports = { log, logError, logInfo, logWarn }

function log(...args) {
  // eslint-disable-next-line no-console
  console.log(...args)
}

function logError(message, error, req) {
  let msg
  if (req) {
    msg =
      new Date().toISOString() +
      ' ' +
      req.ip +
      ' ' +
      req.method +
      ' ' +
      req.path +
      ' ' +
      ' ERROR ' +
      message
  } else {
    msg = new Date().toISOString() + ' ERROR ' + message
  }
  // eslint-disable-next-line no-console
  console.error(msg, error)
}

function logInfo(...args) {
  // eslint-disable-next-line no-console
  console.info(...args)
}

function logWarn(...args) {
  // eslint-disable-next-line no-console
  console.warn(...args)
}
