const { logInfo, logError } = require('./log')

module.exports = async function sendMessage(phoneNumber, message) {
  if (process.env.NODE_ENV !== 'production') {
    logInfo(`Message to ${phoneNumber}: ${message}`)
    return { sent: true, status: 'ok' }
  }

  /*
  const status = logic to send message
  return status
  */

  logError(`Error sending message to ${phoneNumber}: ${message}`)
  return { sent: false, status: 'No valid number format' }
}
