const { getDb } = require('../db/connection')

module.exports = getCollections

async function getCollections() {
  const db = await getDb()

  return {
    Sms: db.collection('sms'),
  }
}
